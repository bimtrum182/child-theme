<?php 

/*
* Create Shortcode
*/
function shortcode_function(  ) {
	
	ob_start();

	get_template_part( './templates/filter');

	return ob_get_clean();
}
add_shortcode( 'navigator-filter-shortcode','shortcode_function');



/*
* Ajax Filter Select Option
*/
add_action( "wp_ajax_ajax_filter", "ajax_filter");
add_action( "wp_ajax_nopriv_ajax_filter", "ajax_filter");
function ajax_filter() {
	$taxData = $_POST['data']['tax'];
	$tagData = $_POST['data']['tag'];
	$minPriceData = $_POST['data']['min'];
	$maxPriceData = $_POST['data']['max'];
	$args = array( 
	   	'post_type' => 'product',
	   	'post_status' => 'publish',
	   	'posts_per_page' => 10,
		
   	);

	if($taxData){
		$args['product_cat'] = $taxData;
	}
	
   	if($tagData){
		$args['product_tag'] = $tagData;
   	}

	if($minPriceData && $maxPriceData){
		$args['meta_query'] = array(
		'relation' => 'OR',
			array(
				'relation' => 'AND',
					array(
						'key'      => '_regular_price',
						'value'    =>  array($minPriceData , $maxPriceData),
						'type'     => 'NUMERIC',
						'compare'  => 'BETWEEN'
					),
					array(
						'relation' => 'OR',
						array(
							'key'      => '_sale_price',
							'value' => '',
    						'compare' => '='
						),
						array(
							'key'      => '_sale_price',
							'compare'  => 'NOT EXIST'
						)
					)
	        	),
	        array(
					'key'      => '_sale_price',
					'value'    =>  array($minPriceData , $maxPriceData),
					'type'     => 'NUMERIC',
					'compare'  => 'BETWEEN'
	        	)
		);
   	}

     $query = new WP_Query( $args );
		while ( $query->have_posts() ) : $query->the_post(); 
			$price = get_post_meta( get_the_ID(), '_regular_price', true);
			
			echo 
				'<li>
					<a href="'.get_permalink().'" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
						<img class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" src="'. get_the_post_thumbnail().'
						<h2 class="woocommerce-loop-product__title">'.get_the_title().'</h2>
						<span class="price">
						<span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$
						</span>'.$price.'</bdi></span>
						</span>
					</a>
				</li>';
		endwhile; 
	die();
}

?>