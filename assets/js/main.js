;
(function($) {

    $(document).ready(function() {

	// Contact-us button
     
    // $('.addThis_iconContact').on('click', function(e) {
    //     e.preventDefault();
    //     $('.addThis_listSharing').toggleClass('active');
    // })

    // $('.addThis_close').on('click', function(e) {
    //     e.preventDefault();
    //     $('.addThis_listSharing').removeClass('active');
    // })
 
    // Function CallAjax
    function callAjax(inputsValue,whereToShow){
        $.ajax({
        url: custom_object.ajaxurl,
        type: 'POST',
        data: {
            action: 'ajax_filter',
            data: inputsValue,
        },
        success: function(data) {
          $(whereToShow).html(data);
        }
        }); 
    }
    // Ajax filter taxonomies & tags
    var selector = $('#filter-tag, #filter-taxonomy');
    $(selector).change(function(){
        taxVal = $('#filter-taxonomy').val();
        tagVal = $('#filter-tag').val();
        callAjax({'tax':taxVal,'tag':tagVal}, $('ul.products'));
    });

    // Ajax filter price
    var selector = $('.submit-price');
    $(selector).click(function() {
        maxPrice = $('.price-max input').val();
        minPrice = $('.price-min input').val();
        callAjax({'min':minPrice,'max':maxPrice}, $('ul.products'));
    });
})

})(jQuery);