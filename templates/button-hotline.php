<?php 
/*
* Hotline-button on footer
*/
 function childtheme_hotline_fixed( $items ) {
	    ?>
			<div class="hotline-phone-ring-wrap">
				<div class="hotline-phone-ring" >
					<div class="hotline-phone-ring-circle"></div>
						<div class="hotline-phone-ring-circle-fill"></div>
						<div class="hotline-phone-ring-img-circle">
							<a href="#" class="pps-btn-img" rel="nofollow">
								<img class="img_call" src="" alt="Số điện thoại" width="50">
							</a>
						</div>
				</div>
				<div class="hotline-bar">
					<a href="#" rel="nofollow">
						<span class="text-hotline">+ 123 456 789</span>
					</a>
				</div>
			</div>
    	<?php
		}
	add_action( 'wp_footer', 'childtheme_hotline_fixed' );

 ?>