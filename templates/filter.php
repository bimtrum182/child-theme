<?php 
// Variable
$taxonomyName = 'product_cat';
$tagName = 'product_tag';
 ?>

<?php

	$terms = get_terms([
	    'taxonomy' => $taxonomyName,
	    'hide_empty' => false,
	]);
	
 	$tags = get_terms([
 		'taxonomy' => $tagName,
	    'hide_empty' => false,
 	])
?>

<div class="section-filter-navigator">
	<!-- Taxonomy -->
    <div class="section-filter__taxonomy">
    	<label for="filter-taxonomy">Taxonomy</label>
    	<select name="filter-taxonomy" id="filter-taxonomy">
    		<option value="" selected>All</option>
	    	<?php foreach ($terms as $term)  {
	    		echo '<option value="'. $term->slug .'">'. $term->name .'</option>';
	    	}?>
    	</select>
    </div>
    <!-- Tag -->
    <div class="section-filter__tag">
    	<label for="filter-tag">Tag</label>
    	<select name="filter-tag" id="filter-tag">
    		<option value="" selected>All</option>
    		<?php foreach ($tags as $tag)  {
	    		echo '<option value="'. $tag->slug .'">'. $tag->name .'</option>';
	    	}?>
    	</select>
    </div>
    <!-- Price -->
    <div class="section-filter__price">
    	<label>Price</label>
    	<div class="price-range">
	    	<div class="price-min">
	    		<input type="number" min="1" maxlength="13" placeholder="$ FROM" value="">
	    	</div>
	    	<div class="price-max">
	    		<input type="number" min="1" maxlength="13" placeholder="$ TO" value="">
	    	</div>
    		<button class="submit-price">Submit</button>
    	</div>
    </div>
</div>