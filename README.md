# child-theme for WordPress

Dưới đây là những hướng dẫn tạo Theme child và tích hợp sẵn cho một website hoàn chỉnh.

## BẮT ĐẦU

Những hướng dẫn dưới đây sẽ cung cấp cho bạn một bản sao của dự án và chạy trên máy cục bộ của bạn với mục đích phát triển và thử nghiệm. Xem triển khai để biết các lưu ý về cách triển khai dự án trên hệ thống.

## CÁC CÔNG CỤ CẦN THIẾT

Dự án này chạy trên máy cục bộ của bạn với mục đích phát triển và thử nghiệm cần có:
- Sever ảo: Wamp hoặc Xampp.
- Trình soạn thảo code: Sublime Text hoặc Visual Studio Code.
- Tài khoản hệ thống quản lí mã nguồn: Gitlab.

## HƯỚNG DẪN CLONE CODE VỀ MÁY CÁ NHÂN

Để lấy được code từ Gitlab về máy tính của bạn, bạn cần làm theo các bước sau đây:
1. Đi đến trang https://gitlab.com/bimtrum182/child-theme/-/tree/main
2. Nhấn vào nút "Clone" rồi copy đường dẫn "Clone with HTTPS" của Repository.
3. Mở màn hình Terminal trên máy tính của bạn, di chuyển đến thư mục bạn muốn chứa các Repository (sử dụng lệnh cd).

    - Lưu ý: Thư mục trên Repository chỉ là Theme child, bạn cần trỏ đúng vào (wp-content\themes) để Clone.
4. Dùng lệnh git clone để sao chép Repository về máy của mình:

    ```
    git clone https://gitlab.com/bimtrum182/child-theme.git
    ```
5. Mở thư mục vừa sao chép về, chúng ta sẽ thấy các file nằm trong đó.
Trên đây chúng tôi đã hướng dẫn bạn Clone dự án từ Gitlab bằng cách sử dụng câu lệnh git clone để sao chép một Repository có sẵn.

## CÀI ĐẶT
1. Sau khi đã hoàn thành "Clone" về máy cá nhân.
2. Bạn tìm đến file "style.css" (child-theme/style.css) và sửa tên của "Template" thành tên của Theme cha mà bạn đang sử dụng.

    - ví dụ: bạn đang sử dụng theme cha là theme Astra thì bạn cần phải sửa mục "Template: hello-elementor" -> "Template: astra"

    - ví dụ: test 123
- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## Hướng dẫn sử dụng hàm lọc taxonomy ,tag, price với AJAX
1. Ở thư mục tempaltes/filter.php : đây là file giao diện sẽ hiển thị giao diện các mục chọn danh mục (select option) và nơi nhập giá trị (input) để 	lọc sản phẩm. File này có hàm chính là get_terms() (Dùng để lấy ra tất cả tên của category (danh mục) và tag(nhãn) ).

	Ví dụ : Bạn có 1 Taxonomy (Loại) tên là Product. Trong Taxonomy Product gồm có những danh mục như Quần Jean, Áo Jean... để lấy các giá trị Quần Jean, Áo Jean... bạn làm như sau:

		- Gán tên taxonomy vào biến đã tạo sẵn ở đầu file filter.php (ở ví dụ này tên taxonomy là Product):
			$taxonomyName = 'Product';

		- Tiếp theo hàm lấy giá trị sẽ được chạy:
			$terms = get_terms([
			    'taxonomy' => $taxonomyName,
			    'hide_empty' => false,
			]);

		- Cuối cùng bạn sẽ có được 1 mảng giá trị muốn lấy.

2. Tiếp theo sẽ tới hàm xử lý AJAX ở file js/custom.js (Trong này sẽ có 3 hàm Function callAjax, Ajax filter taxonomies & tags và Ajax filter price)	
	+ Đầu tiên là hàm callAjax():

		function callAjax(inputsValue,whereToShow){
	        $.ajax({
	        url: custom_object.ajaxurl,
	        type: 'POST',
	        data: {
	            action: 'ajax_filter',
	            data: inputsValue,
	        },
	        success: function(data) {
	          $(whereToShow).html(data);
	        }
	        }); 
    	}

    	- Hàm callAjax() có 2 tham số đầu vào là "inputsValue" và "whereToShow".
    		. inputsValue: là dữ liệu mà bạn muốn ajax gửi đi để xử lý.
    			Ví dụ: ở Bước 1 bạn đã lấy được các giá trị như "Quần jean", "Áo jean". Sau đó bạn chọn "Quần jean" là sản phẩm mà bạn muốn tìm kiếm thì tương đương "inputsValue" = "Quần Jean". Sau đó dữ liệu là "Quần Jean" sẽ được gửi tới server để xử lý.

    		.whereToShow: là nơi dữ liệu sau khi được xử lý sẽ trả về kết quả và sẽ hiển thị ngoài giao diện website của bạn.
    			Ví dụ: callAjax(inputsValue,whereToShow) <=> callAjax("Quần Jean", $('show-result')) và giá trị Trả về là "Quần Jean MS01" sẽ hiển thị ở giao diện là :  

    			<div class="show-result">Quần Jean MS01</div>
    + Tiếp theo là hàm Ajax filter taxonomies & tags:

    	var selector = $('select1, select2');
    	Biến selector sẽ là select bạn muốn chọn để lấy giá trị
    	Ví dụ: 

    			
