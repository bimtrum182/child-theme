<?php

/** 
* @author Anhdt
* @date 03-01-2023
* Add file .css, .js, libraries
**/

function childtheme_configurator_css()
{
    // Hotline button
    // wp_enqueue_style('hotline-style', trailingslashit(get_stylesheet_directory_uri()) . '/assets/css/hotline-button.css', array(), '1.0.0', true);

    // Contact-us button
    // wp_enqueue_style('contact-us-btn-style', trailingslashit(get_stylesheet_directory_uri()) . '/assets/css/contact-us-button.css', array(), '1.0.0', true);

    // AJAX Filter
    wp_enqueue_style('filter-style', trailingslashit(get_stylesheet_directory_uri()) . '/assets/css/filter.css', array(), '1.0.0', true);
    
    // Font Awesome 6 Pro
    // wp_enqueue_style('fontawesome-6-pro', trailingslashit(get_stylesheet_directory_uri()) . '/libraries/fontawesome-6-pro/css/all.min.css', array(), '1.0.0', true);

    wp_enqueue_style('main-style', trailingslashit(get_stylesheet_directory_uri()) . '/assets/css/main.css', array(), '1.0.0', true);

    // Add Js
    wp_enqueue_script('main-js', trailingslashit(get_stylesheet_directory_uri()) . '/assets/js/main.js', array('jquery'));

    // Ajax Admin Url
    wp_localize_script( 
        'custom', 'custom_object',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );

}

add_action('wp_enqueue_scripts', 'childtheme_configurator_css', 10);



/** 
* @author Anhdt
* @date 03-01-2023
* Require Function .php
**/

// require get_stylesheet_directory() . '/templates/hotline-button.php';
// require get_stylesheet_directory() . '/templates/contact-us-button.php';
require get_stylesheet_directory() . '/includes/custom-function.php';

